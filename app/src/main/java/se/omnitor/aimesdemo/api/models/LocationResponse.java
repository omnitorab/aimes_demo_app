package se.omnitor.aimesdemo.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LocationResponse extends ApiResponse {
    @JsonProperty
    public String error;
}
