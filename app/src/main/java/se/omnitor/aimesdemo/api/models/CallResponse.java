package se.omnitor.aimesdemo.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CallResponse extends ApiResponse {
    @JsonProperty
    public String videourl;
    @JsonProperty
    public String emergencynumber;
}
