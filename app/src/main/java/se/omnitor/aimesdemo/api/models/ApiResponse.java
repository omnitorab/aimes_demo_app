package se.omnitor.aimesdemo.api.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiResponse {
    public enum ApiResponseType {
        success,
        failure
    }

    @JsonProperty
    public ApiResponseType result;

    @JsonProperty
    public String error;

    /**
     * Method to convert an object to String.
     * @return
     */
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }
}