package se.omnitor.aimesdemo.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LocationRequest extends ApiRequest {
    @JsonProperty("longitude")
    public double longitude;

    @JsonProperty("latitude")
    public double latitude;

    @JsonProperty("radius")
    public double radius;

    public LocationRequest() {
        super("/api/pemea/location");
    }
}
