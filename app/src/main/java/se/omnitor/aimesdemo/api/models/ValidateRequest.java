package se.omnitor.aimesdemo.api.models;

public class ValidateRequest extends ApiRequest{

    public ValidateRequest() {
        super("/api/validate");
    }
}