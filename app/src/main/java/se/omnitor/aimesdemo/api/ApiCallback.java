package se.omnitor.aimesdemo.api;

import se.omnitor.aimesdemo.api.models.ApiResponse;

public interface ApiCallback {
    public void response(ApiResponse apiResponse);
    public void notAuthenticated();
}
