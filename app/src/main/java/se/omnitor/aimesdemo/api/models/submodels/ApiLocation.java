package se.omnitor.aimesdemo.api.models.submodels;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiLocation {
    @JsonProperty("longitude")
    public double longitude;

    @JsonProperty("latitude")
    public double latitude;

    @JsonProperty("radius")
    public double radius;
}
