package se.omnitor.aimesdemo.api.models;

public class HangupRequest extends ApiRequest {

    public HangupRequest() {
        super("/api/pemea/hangup");
    }
}
