package se.omnitor.aimesdemo.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RegisterRequest extends ApiRequest {
    @JsonProperty
    public String phoneNumber;
    @JsonProperty
    public boolean debug;

    public RegisterRequest() {
        super("/api/register/citizen");
    }
}