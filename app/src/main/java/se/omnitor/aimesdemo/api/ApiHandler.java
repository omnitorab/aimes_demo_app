package se.omnitor.aimesdemo.api;

import se.omnitor.aimesdemo.api.models.ApiRequest;
import se.omnitor.aimesdemo.api.models.ApiResponse;
import se.omnitor.aimesdemo.api.models.CallRequest;
import se.omnitor.aimesdemo.api.models.CallResponse;
import se.omnitor.aimesdemo.api.models.RegisterRequest;
import se.omnitor.aimesdemo.api.models.RegisterResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ApiHandler {
    private String host;
    private String auth;

    /**
     * Creates an ApiHandler with a specified host.
     * @param host URL host to API.
     */
    public ApiHandler(String host) {
        this.host = host;
    }

    /**
     * Send an authHttp request. This method uses the POST method.
     * @param apiRequest API request.
     * @param callback API response callback.
     */
    public void sendRequest(ApiRequest apiRequest, ApiCallback callback) {
        Thread backgroundThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ApiResponse apiResponse = sendThread(apiRequest, null, true);
                    callback.response(apiResponse);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    callback.notAuthenticated();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        backgroundThread.start();
    }

    /**
     * Send an authHttp request. This method uses the POST method.
     * @param apiRequest API request.
     * @param authHeader authorization header.
     * @param callback API response callback.
     */
    public void sendAuthRequest(ApiRequest apiRequest, String authHeader, ApiCallback callback) {
        Thread backgroundThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ApiResponse apiResponse = sendThread(apiRequest, authHeader, true);
                    callback.response(apiResponse);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    callback.notAuthenticated();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        backgroundThread.start();
    }

    /**
     * Send an authHttp request. This method uses the GET method instead of POST.
     * @param apiRequest API request.
     * @param authHeader authorization header.
     * @param callback API response callback.
     */
    public void sendAuthGetRequest(ApiRequest apiRequest, String authHeader, ApiCallback callback) {
        Thread backgroundThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ApiResponse apiResponse = sendThread(apiRequest, authHeader, false);
                    callback.response(apiResponse);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    callback.notAuthenticated();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        backgroundThread.start();
    }

    /**
     * Method to send request to the API.
     * @param apiRequest API request.
     * @param authHeader authorization header.
     * @param isPost boolean to check if POST or GET.
     * @return
     * @throws IOException
     */
    private ApiResponse sendThread(ApiRequest apiRequest, String authHeader, boolean isPost) throws IOException {
        System.out.println("API send request: " + apiRequest.toString());
        URL url = new URL(this.host + apiRequest.getDestinationPath());
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestMethod("GET");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        if(authHeader != null){
            System.out.println("AuthToken: " + authHeader);
            connection.setRequestProperty("Authorization", authHeader);
        }

        // Only send data if POST.
        if(isPost){
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            // Write data.
            OutputStream os = connection.getOutputStream();
            byte[] input = apiRequest.toString().getBytes("utf-8");
            os.write(input, 0, input.length);
            os.close();
        }

        // Read data.
        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
        StringBuilder response = new StringBuilder();
        String responseLine = null;
        while ((responseLine = br.readLine()) != null) {
            response.append(responseLine.trim());
        }
        String responseString = response.toString();

        // Convert string to object.
        ObjectMapper mapper = new ObjectMapper();

        // Determine return object type.
        ApiResponse apiResponse = mapper.readValue(responseString, ApiResponse.class);
        if(apiRequest instanceof RegisterRequest)
            apiResponse = mapper.readValue(responseString, RegisterResponse.class);
        if(apiRequest instanceof CallRequest)
            apiResponse = mapper.readValue(responseString, CallResponse.class);
        return apiResponse;
    }
}