package se.omnitor.aimesdemo.api.models;

import se.omnitor.aimesdemo.api.models.submodels.ApiLanguage;
import se.omnitor.aimesdemo.api.models.submodels.ApiLocation;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.LinkedList;
import java.util.List;

public class CallRequest extends ApiRequest {
    @JsonProperty
    public String name;
    @JsonProperty
    public String phone;
    @JsonProperty
    public List<ApiLanguage> languages;
    @JsonProperty
    public ApiLocation location;

    /**
     * Set up connection to the correct API location.
     */
    public CallRequest() {
        super("/api/pemea/call");
        // Instance list.
        this.languages = new LinkedList<ApiLanguage>();
    }

    /**
     * Not used method to add languages.
     * @param type
     * @param languageCode
     */
    public void addLanguage(ApiLanguage.LanguageType type, String languageCode) {
        ApiLanguage apiLanguage = new ApiLanguage();
        apiLanguage.langtype = type;
        apiLanguage.langcode = languageCode;
        this.languages.add(apiLanguage);
    }
}
