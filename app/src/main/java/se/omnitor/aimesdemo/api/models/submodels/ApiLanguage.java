package se.omnitor.aimesdemo.api.models.submodels;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiLanguage {
    // Holds the language type of a particular language.
    public enum LanguageType {
        spoken,
        written,
        sign
    };

    @JsonProperty("langtype")
    public LanguageType langtype;

    @JsonProperty("langcode")
    public String langcode;

    /**
     * Converts the String langType to enum.
     * @param langType
     */
    public void langTypeFromString(String langType){
        if(langType.equals("Spoken"))
            this.langtype = LanguageType.spoken;
        else if (langType.equals("Written"))
            this.langtype = LanguageType.written;
        else
            this.langtype = LanguageType.sign;
    }

    /**
     * Converts the enum langType to String.
     * @return Returned String.
     */
    public String langTypeToString(){
        if(this.langtype == LanguageType.spoken)
            return "Spoken";
        else if (this.langtype == LanguageType.written)
            return "Written";
        else
            return "Sign";
    }
}
