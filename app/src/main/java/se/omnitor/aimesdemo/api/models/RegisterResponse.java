package se.omnitor.aimesdemo.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RegisterResponse extends ApiResponse {
    @JsonProperty
    public String salt;
    @JsonProperty
    public String secreta;
}