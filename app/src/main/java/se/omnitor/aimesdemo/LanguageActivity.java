package se.omnitor.aimesdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import se.omnitor.aimesdemo.R;

import se.omnitor.aimesdemo.api.models.submodels.ApiLanguage;
import se.omnitor.aimesdemo.utils.AlertBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class LanguageActivity extends AppCompatActivity {

    final LanguageActivity languageActivity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);

        Button btnAddLanguage = this.findViewById(R.id.btnAddLanguage);
        EditText edtTextAddLanguage = this.findViewById(R.id.edtTextAddLanguage);
        LinearLayout lstLanguage = this.findViewById(R.id.lstLanguage);

        // Load data.
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String allLanguagesString = pref.getString("aimes.languages", "");

        ObjectMapper mapper = new ObjectMapper();
        ApiLanguage[] allLanguages = new ApiLanguage[0];
        try {
            allLanguages = mapper.readValue(allLanguagesString, ApiLanguage[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (ApiLanguage lang : allLanguages) {
            createListItem(lstLanguage, lang.langcode, lang.langTypeToString());
        }

        btnAddLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtTextAddLanguage.getEditableText().toString().length() == 0)
                    AlertBuilder.displayAlert(R.string.alert_error_header, R.string.alert_missing_language_field, languageActivity);
                else {
                    AlertBuilder.displayThreeAlternativeAlert(LanguageActivity.this, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String userInput = edtTextAddLanguage.getEditableText().toString();
                            if (which == DialogInterface.BUTTON_POSITIVE) {
                                createListItem(lstLanguage, userInput, "Spoken");
                            } else if (which == DialogInterface.BUTTON_NEUTRAL) {
                                createListItem(lstLanguage, userInput, "Written");
                            } else {
                                createListItem(lstLanguage, userInput, "Sign");
                            }
                            //edtTextAddLanguage.getEditableText().clear();
                        }
                    });
                }
            }
        });
    }

    /**
     * Method to save entered language information in a list.
     *
     * @param parent              The view to add newly created list items.
     * @param langCode            Used to store the language code.
     * @param communicationMethod Used to store the communication method.
     */
    private void createListItem(LinearLayout parent, String langCode, String communicationMethod) {
        ConstraintLayout newLanguage = (ConstraintLayout) getLayoutInflater().inflate(R.layout.language_list_item, null);
        TextView txtViewLangCode = newLanguage.findViewById(R.id.txtViewLangCode);
        TextView txtViewCommunication = newLanguage.findViewById(R.id.txtViewCommunication);
        Button btnRemoveItem = newLanguage.findViewById(R.id.btnRemoveItem);
        btnRemoveItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parent.removeView(newLanguage);
            }
        });
        txtViewLangCode.setText(langCode);
        txtViewCommunication.setText(communicationMethod);

        parent.addView(newLanguage);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Save data.
        LinearLayout lstLanguage = this.findViewById(R.id.lstLanguage);
        List<ApiLanguage> allLanguages = new LinkedList<ApiLanguage>();
        // Go through all children.
        for (int i = 0; i < lstLanguage.getChildCount(); i++) {
            final View child = lstLanguage.getChildAt(i);
            TextView txtViewLangCode = child.findViewById(R.id.txtViewLangCode);
            TextView txtViewCommunication = child.findViewById(R.id.txtViewCommunication);

            ApiLanguage apiLanguage = new ApiLanguage();
            apiLanguage.langcode = txtViewLangCode.getText().toString();
            apiLanguage.langTypeFromString(txtViewCommunication.getText().toString());

            allLanguages.add(apiLanguage);
        }

        ObjectMapper mapper = new ObjectMapper();
        String savedLanguages = "";
        try {
            savedLanguages = mapper.writeValueAsString(allLanguages);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("aimes.languages", savedLanguages);
        editor.apply();
    }
}