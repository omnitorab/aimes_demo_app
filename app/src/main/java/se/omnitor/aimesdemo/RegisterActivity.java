package se.omnitor.aimesdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import se.omnitor.aimesdemo.api.ApiCallback;
import se.omnitor.aimesdemo.api.ApiHandler;
import se.omnitor.aimesdemo.api.models.ApiResponse;
import se.omnitor.aimesdemo.api.models.RegisterRequest;
import se.omnitor.aimesdemo.api.models.RegisterResponse;
import se.omnitor.aimesdemo.api.models.ValidateRequest;
import se.omnitor.aimesdemo.utils.AlertBuilder;
import se.omnitor.aimesdemo.utils.HexConverter;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = RegisterActivity.class.getSimpleName();
    private String salt;
    private String secretA;
    private String savedLanguages;

    // Save views as instance.
    EditText edtTextPhone;
    EditText edtTextFirstName;
    EditText edtTextLastName;
    EditText edtTextCode;
    TextView txtViewAppVersion;
    Button btnLanguageSelect;
    Button btnRegisterFinish;
    Button btnUnRegister;
    Button btnSendCode;

    final RegisterActivity registerActivity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ApiHandler handler = new ApiHandler("https://mw.aimes-eurostars.eu");

        // Load elements.
        btnSendCode = this.findViewById(R.id.btnSendCode);
        btnRegisterFinish = this.findViewById(R.id.btnRegisterFinish);
        btnLanguageSelect = this.findViewById(R.id.btnLanguageSelect);
        btnUnRegister = this.findViewById(R.id.btnUnRegister);
        edtTextFirstName = this.findViewById(R.id.edtTextFirstName);
        edtTextLastName = this.findViewById(R.id.edtTextLastName);
        edtTextPhone = this.findViewById(R.id.edtTextPhone);
        edtTextCode = this.findViewById(R.id.edtTextCode);
        txtViewAppVersion = this.findViewById(R.id.txtViewAppVersion);

        // Display app version number on the register page.
        txtViewAppVersion.setText(BuildConfig.VERSION_NAME);

        // Click listeners.
        btnSendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = edtTextPhone.getEditableText().toString();
                if (!phoneNumber.matches("^[+][0-9]{8,15}$")) {
                    AlertBuilder.displayAlert(getResources().getString(R.string.alert_error_header), getResources().getString(R.string.alert_not_valid_phone_number), registerActivity);
                    return;
                }
                RegisterRequest registerRequest = new RegisterRequest();
                registerRequest.phoneNumber = phoneNumber;
                registerRequest.debug = false;

                // Send request.
                handler.sendRequest(registerRequest, new ApiCallback() {
                    @Override
                    public void response(ApiResponse apiResponse) {
                        System.out.println(apiResponse.toString());
                        if (apiResponse.result == ApiResponse.ApiResponseType.success) {
                            AlertBuilder.displayAlert(getResources().getString(R.string.alert_sms_sent), getResources().getString(R.string.alert_sms_sent_text) + registerRequest.phoneNumber, registerActivity);
                            // Fetch responses from API.
                            secretA = ((RegisterResponse) apiResponse).secreta;
                            salt = ((RegisterResponse) apiResponse).salt;
                            System.out.println("SECRETA: " + secretA);
                            System.out.println("SALT: " + salt);
                        } else {
                            AlertBuilder.displayAlert(getResources().getString(R.string.alert_error_header), apiResponse.error, registerActivity);
                        }
                    }

                    @Override
                    public void notAuthenticated() {
                        // This will never be called because register does not authenticate.
                    }
                });
            }
        });

        btnLanguageSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Starting InCallActivity when pressing Call button.
                Intent intent = new Intent(RegisterActivity.this, LanguageActivity.class);
                startActivity(intent);
            }
        });

        btnRegisterFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(registerActivity);
                savedLanguages = pref.getString("aimes.languages", "");
                if (edtTextFirstName.getEditableText().toString().length() == 0 || edtTextLastName.getEditableText().toString().length() == 0 || edtTextPhone.getEditableText().toString().length() == 0) {
                    AlertBuilder.displayAlert(getResources().getString(R.string.alert_error_header), getResources().getString(R.string.alert_missing_name_fields), registerActivity);
                } else if (!savedLanguages.contains("written") || !savedLanguages.contains("spoken")) {
                    AlertBuilder.displayAlert(getResources().getString(R.string.alert_error_header), getResources().getString(R.string.alert_missing_spoken_written), registerActivity);
                } else {
                    String secretB = edtTextCode.getEditableText().toString();
                    // Re-use the entered phone number to save locally.
                    String phoneNumber = edtTextPhone.getEditableText().toString();

                    String authToken = null;
                    try {
                        authToken = generateHash(secretA + salt + secretB);
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                        // Display error to user.
                        AlertBuilder.displayAlert(getResources().getString(R.string.alert_error_header), e.getMessage(), registerActivity);
                        return;
                    }
                    // Copy auth token to final.
                    final String authTokenFinal = authToken;

                    ValidateRequest validateRequest = new ValidateRequest();

                    handler.sendAuthGetRequest(validateRequest, authToken, new ApiCallback() {
                        @Override
                        public void response(ApiResponse apiResponse) {
                            // Check if successful.
                            if (apiResponse.result == ApiResponse.ApiResponseType.success) {
                                // Save information and close page.
                                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(registerActivity);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putString("aimes.token", authTokenFinal);
                                editor.apply();
                                // Disable fields and report success.
                                disableRegistration(true);
                                AlertBuilder.displayAlert(getResources().getString(R.string.alert_success), getResources().getString(R.string.alert_success_text), registerActivity);
                            } else {
                                // Alert user about error.
                                AlertBuilder.displayAlert(getResources().getString(R.string.alert_error_header), apiResponse.error, registerActivity);
                            }
                        }

                        @Override
                        public void notAuthenticated() {
                            // Alert user wrong secretb.
                            AlertBuilder.displayAlert(getResources().getString(R.string.alert_error_header), getResources().getString(R.string.alert_wrong_pin_entered_text), registerActivity);
                        }
                    });
                }
            }
        });

        btnUnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertBuilder.displayYesNoAlert(R.string.alert_clear_all_header, R.string.alert_clear_all_text, registerActivity, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Clear fields.
                        edtTextFirstName.setText("");
                        edtTextLastName.setText("");
                        edtTextPhone.setText("");

                        // Clear all saved information.
                        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(registerActivity);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.clear().apply();

                        disableRegistration(false);
                    }
                });
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("aimes.firstname", edtTextFirstName.getText().toString());
        editor.putString("aimes.lastname", edtTextLastName.getText().toString());
        editor.putString("aimes.phone", edtTextPhone.getText().toString());
        editor.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Load sharedpref items.
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        edtTextFirstName.setText(pref.getString("aimes.firstname", ""));
        edtTextLastName.setText(pref.getString("aimes.lastname", ""));
        edtTextPhone.setText(pref.getString("aimes.phone", ""));

        // Disable all fields if registered.
        String token = pref.getString("aimes.token", "");
        disableRegistration(token.length() > 0);
    }

    private String generateHash(String unHashedToken) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(unHashedToken.getBytes());
        byte[] digest = md.digest();
        return HexConverter.bytesToHex(digest);
    }

    /**
     * This method disables the registration fields if registration was successful.
     * The fields are enables if the user unregister.
     *
     * @param disabled State variable to track if user is registered.
     */
    private void disableRegistration(boolean disabled) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (disabled) {
                    edtTextFirstName.setEnabled(false);
                    edtTextLastName.setEnabled(false);
                    edtTextPhone.setEnabled(false);
                    btnLanguageSelect.setEnabled(false);
                    btnSendCode.setEnabled(false);
                    edtTextCode.setEnabled(false);
                    btnRegisterFinish.setEnabled(false);
                    btnUnRegister.setEnabled(true);
                } else {
                    edtTextFirstName.setEnabled(true);
                    edtTextLastName.setEnabled(true);
                    edtTextPhone.setEnabled(true);
                    btnLanguageSelect.setEnabled(true);
                    btnSendCode.setEnabled(true);
                    edtTextCode.setEnabled(true);
                    btnRegisterFinish.setEnabled(true);
                    btnUnRegister.setEnabled(false);
                }
            }
        });
    }
}