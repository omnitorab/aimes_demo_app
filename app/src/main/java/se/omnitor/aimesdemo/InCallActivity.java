package se.omnitor.aimesdemo;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.ClipData;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.PermissionRequest;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import se.omnitor.aimesdemo.api.ApiCallback;
import se.omnitor.aimesdemo.api.ApiHandler;
import se.omnitor.aimesdemo.api.models.ApiResponse;
import se.omnitor.aimesdemo.api.models.HangupRequest;
import se.omnitor.aimesdemo.utils.AlertBuilder;

import java.util.LinkedList;
import java.util.List;

public class InCallActivity extends AppCompatActivity {

    private String TAG = InCallActivity.class.getSimpleName();
    public static final int MY_PERMISSIONS_REQUEST = 90;
    public static final int FILE_CHOOSER_RESULT_CODE = 111;
    private ValueCallback<Uri[]> uploadMessageAboveL;
    private String digiroomURL;
    private boolean allPermissionsAccepted = false;
    private boolean isLoaded = false;

    private WebView myWebView;
    final InCallActivity activityContext = this;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_call);

        // Get URL, added extension to skip media-selector.
        this.digiroomURL = getIntent().getExtras().getString(MainActivity.URLEXTRA) + "#automs=true";

        // Fetch webview element.
        myWebView = (WebView) findViewById(R.id.webviewDigiRoom);

        // Set default web settings.
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setAllowFileAccessFromFileURLs(true);
        webSettings.setAllowUniversalAccessFromFileURLs(true);
        webSettings.setMediaPlaybackRequiresUserGesture(false);
        webSettings.setAllowFileAccess(true);

        // Enable debugging.
        WebView.setWebContentsDebuggingEnabled(true);
    }

    /**
     * Check if permissions have been granted. If not, ask for permissions.
     */
    public void checkPermissions() {
        List<String> permissionList = new LinkedList<>();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            permissionList.add(Manifest.permission.CAMERA);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            permissionList.add(Manifest.permission.RECORD_AUDIO);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionList.size() > 0) {
            ActivityCompat.requestPermissions(this, permissionList.toArray(new String[0]), MY_PERMISSIONS_REQUEST);
        } else {
            allPermissionsAccepted = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        myWebView.setWebViewClient(new WebViewClient());

        myWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onCloseWindow(WebView window) {
                super.onCloseWindow(window);
                // User hung up call in DigiRoom.
                activityContext.hangUp();
                activityContext.finish();
                isLoaded = false;
            }

            // Grant permissions.
            @Override
            public void onPermissionRequest(final PermissionRequest request) {
                Log.d(TAG, "onPermissionRequest on page: " + request.getOrigin().toString());
                InCallActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Safe way to grant permissions
                        if (request.getOrigin().toString().matches("https://(\\w+\\.)*digiroom\\.se/")) {
                            for (String permission : request.getResources()) {
                                Log.d(TAG, "Permission granted: " + permission);
                            }
                            request.grant(request.getResources());

                        } else {
                            request.deny();
                        }
                    }
                });
            }

            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                Log.d(TAG, "File-chooser open");
                openImageChooserActivity();
                uploadMessageAboveL = filePathCallback;
                return true;
            }

            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                android.util.Log.d(TAG + " JS", consoleMessage.message());
                return true;
            }
        });

        myWebView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent,
                                        String contentDisposition, String mimetype,
                                        long contentLength) {
                // Initialize a new download request
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                request.allowScanningByMediaScanner();
                request.setDescription("Downloading file...");
                // Split the url to ignore the .html from digiroom and look at the filename.
                String[] temp = url.split("\\?url=");
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                if (temp.length > 1) {
                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(temp[1], contentDisposition, mimetype));
                    DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                    downloadManager.enqueue(request);
                } else {
                    // Should not occur.
                    AlertBuilder.displayAlert(R.string.alert_error_header, R.string.alert_download_error, activityContext);
                }
            }
        });

        // Check if permissions have been granted.
        checkPermissions();

        // Load url if all permissions are granted and not already loaded.
        if (allPermissionsAccepted && !isLoaded) {
            loadURL();
            isLoaded = true;
            CookieManager.getInstance().flush();
        }
    }

    /**
     * Load the DigiRoom URL to the webview.
     */
    public void loadURL() {
        myWebView.loadUrl(this.digiroomURL);
        // Log.d(TAG, "Loaded URL: " + this.digiroomURL);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    /**
     * Send HangUp to the API.
     */
    public void hangUp() {
        ApiHandler handler = new ApiHandler("https://mw.aimes-eurostars.eu");

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String authToken = pref.getString("aimes.token", "");

        handler.sendAuthRequest(new HangupRequest(), authToken, new ApiCallback() {
            @Override
            public void response(ApiResponse apiResponse) {
            }

            @Override
            public void notAuthenticated() {
            }
        });
    }

    /**
     * Open image selector in Android, will return to onActivityResult.
     */
    private void openImageChooserActivity() {
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType("image/*");
        startActivityForResult(Intent.createChooser(i, "Image Chooser"), FILE_CHOOSER_RESULT_CODE);
    }

    /**
     * Result with an image selected.
     *
     * @param requestCode Specifying that this is a image selector result.
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == FILE_CHOOSER_RESULT_CODE) {
            Uri[] results = null;
            if (data != null) {
                String dataString = data.getDataString();
                ClipData clipData = data.getClipData();
                if (clipData != null) {
                    results = new Uri[clipData.getItemCount()];
                    for (int i = 0; i < clipData.getItemCount(); i++) {
                        ClipData.Item item = clipData.getItemAt(i);
                        results[i] = item.getUri();
                    }
                }
                if (dataString != null) {
                    results = new Uri[]{Uri.parse(dataString)};
                }
            }
            Log.d(TAG, "File uploading???");
            uploadMessageAboveL.onReceiveValue(results);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        // Overriding method should call super.onRequestPermissionsResult.
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        final InCallActivity activityContext = this;
        if (requestCode == MY_PERMISSIONS_REQUEST) {
            if (ContextCompat.checkSelfPermission(activityContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(activityContext, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(activityContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                allPermissionsAccepted = true;
            } else {
                AlertBuilder.displayAlert(R.string.alert_error_header, R.string.call_permission_denied_text, this);
            }
        }
    }

    /**
     * Require the user to press "back" twice to exit DigiRoom.
     */
    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            activityContext.hangUp();
            activityContext.finish();
            isLoaded = false;
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        CharSequence toastText = getResources().getString(R.string.toast_exit_call_verification);
        Toast.makeText(this, toastText, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}