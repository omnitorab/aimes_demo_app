package se.omnitor.aimesdemo.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import se.omnitor.aimesdemo.InCallActivity;
import se.omnitor.aimesdemo.R;

/**
 * Class to generate alerts to the user.
 */
public class AlertBuilder {
    /**
     * Creates an alert to display information to the user.
     *
     * @param header  resource integer for a header.
     * @param message resource integer for the message.
     * @param context the android context.
     */
    public static void displayAlert(int header, int message, Context context) {
        displayAlert(context.getResources().getString(header), context.getResources().getString(message), context);
    }

    public static void displayAlert(String header, String message, Context context) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle(header);
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                R.string.alert_ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                // Things need to work on ui thread.
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
    }

    /**
     * Creates an alert to ask the user a yes/no question.
     *
     * @param header   resource integer for a header.
     * @param message  resource integer for the message.
     * @param context  the android context.
     * @param listener listener to the buttons.
     */
    public static void displayYesNoAlert(int header, int message, Context context, DialogInterface.OnClickListener listener) {
        displayYesNoAlert(context.getResources().getString(header), context.getResources().getString(message), context, listener);
    }

    public static void displayYesNoAlert(String header, String message, Context context, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle(header);
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(R.string.alert_yes, listener);
        builder1.setNegativeButton(R.string.alert_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                // Things need to work on ui thread.
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
    }

    /**
     * Pop-up that requests the user to select which communication method the entered language is.
     *
     * @param context  Used to create and run UI elements.
     * @param listener OnClickListener to react to the selected language type.
     */
    public static void displayThreeAlternativeAlert(Context context, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle(R.string.alert_select_language_type);
        builder1.setMessage(R.string.alert_select_communication_method);
        builder1.setCancelable(true);

        builder1.setPositiveButton(R.string.alert_positive_button, listener);
        builder1.setNeutralButton(R.string.alert_neutral_button, listener);
        builder1.setNegativeButton(R.string.alert_negative_button, listener);

        ((Activity) context).runOnUiThread(new Runnable() {
            public void run() {
                // Things need to work on ui thread.
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });
    }
}
