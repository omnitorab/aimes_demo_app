package se.omnitor.aimesdemo.utils.user_data;

/**
 * Simple structure to hold Language.
 */
public class Language {
    private String language;
    private String code;
    private Type type;
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setType(Type t){
        this.type = t;
    }
    public Type getType(){
        return this.type;
    }
    public Language(String lang, String lcode){
        language = lang;
        code = lcode;
        this.type = Type.SPOKEN;
    }
    public Language(String lang, String lcode, Type type){
        language = lang;
        code = lcode;
        this.type = type;
    }

    /**
     * Language type can be spoken, written or sign.
     */
    public enum Type{
        SPOKEN, WRITTEN, SIGN
    }

    @Override
    public String toString() {
        return this.language;
    }
}