package se.omnitor.aimesdemo.utils.user_data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import se.omnitor.aimesdemo.RegisterActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * Class to utilize the NEXES language database.
 */
public class LanguageDatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = RegisterActivity.class.getSimpleName();

    private static final String DATABASE_NAME = "nexesLanguages.db";
    private static String dbPath = "/data/data/nexes.eu.uilib/database/";

    private static final int DATABASE_VERSION = 1;
    private final String assetPath;
    private SQLiteDatabase myDataBase;
    private Context mContext;

    public LanguageDatabaseHelper(Context context) throws IOException {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
        this.assetPath = DATABASE_NAME;
        this.dbPath = "/data/data/"
                + context.getApplicationContext().getPackageName() + "/databases/"
                + DATABASE_NAME;
        checkExists();
    }
    /**
     * Checks if the database asset needs to be copied and if so copies it to the
     * default location.
     *
     * @throws IOException
     */
    private void checkExists() throws IOException {
        Log.i(TAG, "checkExists()");

        File dbFile = new File(dbPath);

        if (!dbFile.exists()) {

            Log.i(TAG, "creating database..");

            dbFile.getParentFile().mkdirs();
            copyStream(mContext.getAssets().open(assetPath), new FileOutputStream(
                    dbFile));

            Log.i(TAG, assetPath + " has been copied to " + dbFile.getAbsolutePath());
        }

    }

    private void copyStream(InputStream is, OutputStream os) throws IOException {
        byte buf[] = new byte[1024];
        int c = 0;
        while (true) {
            c = is.read(buf);
            if (c == -1)
                break;
            os.write(buf, 0, c);
        }
        is.close();
        os.close();
    }


    public ArrayList<Language> getLanguages() {
        SQLiteDatabase db = getReadableDatabase();
        Log.d("isOpen", String.valueOf(db.isOpen()));
        //SQLiteStatement s = db.compileStatement("SELECT * FROM sqlite_master WHERE name ='tablename' and type='table';");
        //long count = s.simpleQueryForLong();
        //Log.d("isOpen",String.valueOf(count));
        ArrayList<Language> list = new ArrayList<Language>();
        // Select All Query
        String selectQuery = "SELECT Languages.code, Descriptions.description\n" +
                "FROM Languages " +
                "JOIN Descriptions_Languages ON (Languages.code = Descriptions_Languages.lang)\n" +
                "JOIN Descriptions ON (Descriptions_Languages.description = Descriptions.id)\n" +
                "WHERE Descriptions.description NOT LIKE '%sign%' " +
                "ORDER BY Languages.added ASC";
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Language lang = new Language(cursor.getString(1), cursor.getString(0));
                // Adding contact to list
                list.add(lang);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }
    public ArrayList<Language> getSignLanguages() {
        SQLiteDatabase db = getReadableDatabase();
        Log.d("isOpen", String.valueOf(db.isOpen()));
        ArrayList<Language> list = new ArrayList<Language>();
        // Select All Query
        String selectQuery = "SELECT Languages.code, Descriptions.description\n" +
                "FROM Languages " +
                "JOIN Descriptions_Languages ON (Languages.code = Descriptions_Languages.lang)\n" +
                "JOIN Descriptions ON (Descriptions_Languages.description = Descriptions.id)\n" +
                "WHERE Descriptions.description LIKE '%sign%' " +
                "ORDER BY Languages.added ASC";
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Language lang = new Language(cursor.getString(1),cursor.getString(0), Language.Type.SIGN);
                // Adding contact to list
                list.add(lang);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }
    public ArrayList<Language> getWrittenLanguages() {
        SQLiteDatabase db = getReadableDatabase();
        Log.d("isOpen", String.valueOf(db.isOpen()));
        ArrayList<Language> list = new ArrayList<Language>();
        String selectQuery = "SELECT Languages.code, Descriptions.description\n" +
                "FROM Languages " +
                "JOIN Descriptions_Languages ON (Languages.code = Descriptions_Languages.lang)\n" +
                "JOIN Descriptions ON (Descriptions_Languages.description = Descriptions.id)\n" +
                //"WHERE Languages.suppressscript IS NOT NULL " +
                "WHERE Descriptions.description NOT LIKE '%sign%' " +
                "ORDER BY Languages.added ASC";
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Language lang = new Language(cursor.getString(1),cursor.getString(0), Language.Type.WRITTEN);
                // Adding contact to list
                list.add(lang);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return list;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}