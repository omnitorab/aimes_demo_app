package se.omnitor.aimesdemo.utils;

import java.nio.charset.StandardCharsets;

/**
 * A utility class to hold static methods
 * for converting byte to string hex.
 */
public class HexConverter {

    // Needs a valid array with HEX values.
    private static final byte[] HEX_ARRAY = "0123456789ABCDEF".getBytes(StandardCharsets.US_ASCII);

    /**
     * Converts byte to uppercase
     * hex string.
     * @param bytes the bytes to be converted to a string.
     * @return returns an uppercase HEX string.
     */
    public static String bytesToHex(byte[] bytes) {
        byte[] hexChars = new byte[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars, StandardCharsets.UTF_8);
    }

}
