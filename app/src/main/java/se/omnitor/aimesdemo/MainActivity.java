package se.omnitor.aimesdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import se.omnitor.aimesdemo.R;

import se.omnitor.aimesdemo.api.ApiCallback;
import se.omnitor.aimesdemo.api.ApiHandler;
import se.omnitor.aimesdemo.api.models.ApiResponse;
import se.omnitor.aimesdemo.api.models.CallRequest;
import se.omnitor.aimesdemo.api.models.CallResponse;
import se.omnitor.aimesdemo.api.models.LocationRequest;
import se.omnitor.aimesdemo.api.models.submodels.ApiLanguage;
import se.omnitor.aimesdemo.api.models.submodels.ApiLocation;
import se.omnitor.aimesdemo.utils.AlertBuilder;
import se.omnitor.aimesdemo.utils.LocationUpdatesService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements LocationUpdatesService.LocationUpdateListener, LocationSource {
    public static final String URLEXTRA = "digiroomURL";
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public GoogleMap googleMap;
    private MainActivity mInstance;
    private String TAG = MainActivity.class.getSimpleName();
    private Location mCurrentLocation;
    private OnLocationChangedListener mGoogleMapLocationListener;

    private Button btnPlaceCall;

    private CallRequest callRequest = new CallRequest();
    private String authToken;

    private boolean inCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ApiHandler handler = new ApiHandler("https://mw.aimes-eurostars.eu");

        // Load map.
        MapView mapView = this.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);

        // Load buttons.
        btnPlaceCall = this.findViewById(R.id.btnPlaceCall);
        Button btnRegister = this.findViewById(R.id.btnRegister);

        // Click listeners.
        btnPlaceCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mCurrentLocation != null) {
                    // Get DigiRoom URL from API.
                    handler.sendAuthRequest(callRequest, authToken, new ApiCallback() {
                        @Override
                        public void response(ApiResponse apiResponse) {
                            System.out.println("API Response: " + apiResponse.toString());
                            // Starting InCallActivity when pressing Call button.
                            Intent intent = new Intent(MainActivity.this, InCallActivity.class);
                            intent.putExtra(URLEXTRA, ((CallResponse) apiResponse).videourl);
                            startActivity(intent);

                            // We are in call now.
                            inCall = true;
                        }

                        @Override
                        public void notAuthenticated() {
                        }
                    });
                    Toast.makeText(MainActivity.this, R.string.toast_placing_call, Toast.LENGTH_LONG).show();
                    // Disable the call button for 5s after making a call to avoid repeated calls.
                    btnPlaceCall.setEnabled(false);
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            btnPlaceCall.setEnabled(true);
                        }
                    }, 5000);
                } else {
                    Toast.makeText(MainActivity.this, R.string.toast_no_location, Toast.LENGTH_LONG).show();
                }

            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Starting RegisterActivity when pressing the Register button.
                Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
        mInstance = this;

        // Request location permissions if not already granted.
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        final MainActivity activityContext = this;

        // On resume we are not in call.
        this.inCall = false;

        // Load data.
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        this.authToken = pref.getString("aimes.token", "");
        this.btnPlaceCall.setEnabled(this.authToken.length() > 0);

        this.callRequest.name = pref.getString("aimes.firstname", "") + " " + pref.getString("aimes.lastname", "");
        this.callRequest.phone = "tel:" + pref.getString("aimes.phone", "");

        String allLanguagesString = pref.getString("aimes.languages", "");

        ObjectMapper mapper = new ObjectMapper();
        List<ApiLanguage> allLanguages = new LinkedList<>();
        if (allLanguagesString.length() > 0) {
            try {
                allLanguages = Arrays.asList(mapper.readValue(allLanguagesString, ApiLanguage[].class));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.callRequest.languages = allLanguages;

        // Load map.
        MapView mapView = this.findViewById(R.id.mapView);
        mapView.onResume();

        // Enable map to show user location. Needed to be in onResume instead of onCreate in order to be called correctly.
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                activityContext.googleMap = googleMap;
                googleMap.setLocationSource(mInstance);
                if (ContextCompat.checkSelfPermission(mInstance, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(mInstance, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    googleMap.setMyLocationEnabled(true);
                }
                if (mCurrentLocation != null) {
                    updateMapLocation(mCurrentLocation);
                }
            }
        });

        // Bind the location update service.
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            doBindService();
        }
    }

    @Override
    protected void onDestroy() {
        mInstance = null;
        // Unbind the location service when closing the app.
        doUnbindService();
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        final MainActivity activityContext = this;
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (ContextCompat.checkSelfPermission(activityContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(activityContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                doBindService();
            } else {
                AlertBuilder.displayAlert(R.string.alert_error_header, R.string.location_permission_denied_text, this);
            }
        }
    }

    private LocationUpdatesService mLocationService;
    private boolean mLocationBound;
    private final ServiceConnection mLocationServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationUpdatesService.LocalBinder binder = (LocationUpdatesService.LocalBinder) service;
            mLocationService = binder.getService();
            mLocationBound = true;
            mLocationService.requestLocationUpdates();
            mLocationService.setLocationListener(mInstance);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mLocationService = null;
            mLocationBound = false;
        }
    };

    /**
     * Bind to the service. If the service is in foreground mode, this signals to the service
     * that since this activity is in the foreground, the service can exit foreground mode.
     */
    void doBindService() {
        bindService(new Intent(this, LocationUpdatesService.class), mLocationServiceConnection,
                Context.BIND_AUTO_CREATE);
    }

    /**
     * Unbind from the service. This signals to the service that this activity is no longer
     * in the foreground, and the service can respond by promoting itself to a foreground
     * service.
     */
    void doUnbindService() {
        if (mLocationBound) {
            mLocationService.removeLocationUpdates();
            unbindService(mLocationServiceConnection);
        }
    }

    @Override
    public void onLocationUpdate(Location location) {
        Log.d(TAG, "Got location: " + location);
        ApiLocation apiLocation = new ApiLocation();
        apiLocation.latitude = location.getLatitude();
        apiLocation.longitude = location.getLongitude();
        apiLocation.radius = location.getAccuracy();

        if (this.inCall) {
            // We are in call, send an location update.
            LocationRequest locationRequest = new LocationRequest();
            locationRequest.latitude = location.getLatitude();
            locationRequest.longitude = location.getLongitude();
            locationRequest.radius = location.getAccuracy();

            ApiHandler handler = new ApiHandler("https://mw.aimes-eurostars.eu");
            handler.sendAuthRequest(locationRequest, this.authToken, new ApiCallback() {
                @Override
                public void response(ApiResponse apiResponse) {
                }

                @Override
                public void notAuthenticated() {
                }
            });
        }

        this.callRequest.location = apiLocation;

        mCurrentLocation = location;
        updateMapLocation(location);
        if (mGoogleMapLocationListener != null)
            mGoogleMapLocationListener.onLocationChanged(location);
    }

    /**
     * Update map location, center on location and zoom in.
     *
     * @param location New location provided by the app.
     */
    private void updateMapLocation(Location location) {
        if (location != null && googleMap != null) {
            LatLng myLocation = new LatLng(location.getLatitude(),
                    location.getLongitude());
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 12));
        }
    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {
        mGoogleMapLocationListener = onLocationChangedListener;
    }

    @Override
    public void deactivate() {
        mGoogleMapLocationListener = null;
    }

    boolean doubleBackToExitPressedOnce = false;

    /**
     * Require the user to press "back" twice to exit the application.
     */
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        CharSequence toastText = getResources().getString(R.string.toast_exit_app_verification);
        Toast.makeText(this, toastText, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}