[[_TOC_]]

# Introduction 
The AIMES Demo App is used to demonstrate the capabilities and functions developed in the AIMES project.

This project will also give third-party developers an example of how to integrate the AIMES functionality in a new or existing app. 

# Getting Started
This project is developed with Android Studio in Java. 

To get the project running on your system:

1. Download Android Studio. (https://developer.android.com/studio)
2. Clone this repository and open it as a project in Android Studio.
3. Build the project.
4. Run the app on a virtual or physical device.

The app is also available on Google Play Store as a closed beta.

1. To access the app please send me (thomas.geiger@omnitor.se) your Google account connected to your Google Play.
2. Enter the following link https://play.google.com/apps/testing/se.omnitor.aimesdemo.
3. Join as internal tester and download the app from there.

# Features

The AIMES demo app consists of four Activities
•	MainActivity
•	InCallActivity
•	RegisterActivity
•	LanguageActivity


# DigiRoom integration into native app
DigiRoom is a SaaS allowing this app to easily start a video call.
It is a WebRTC solution that is accessed through a WebView in the InCallActivity.

DigiRoom requires the following permissions and features to be added in the Manifest.xml.
```xml
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.CAMERA" />
<uses-permission android:name="android.permission.FOREGROUND_SERVICE" />
<uses-permission android:name="android.permission.RECORD_AUDIO" />
<uses-permission android:name="android.permission.MODIFY_AUDIO_SETTINGS" />
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
<uses-permission android:name="android.webkit.PermissionRequest" />
```

Websettings to be set in the InCallActivity’s onCreate:
```Java
WebSettings webSettings = myWebView.getSettings();
webSettings.setJavaScriptEnabled(true);
webSettings.setAllowFileAccessFromFileURLs(true);
webSettings.setAllowUniversalAccessFromFileURLs(true);
webSettings.setMediaPlaybackRequiresUserGesture(false);
webSettings.setDomStorageEnabled(true); 
webSettings.setAllowFileAccess(true);
```

It is required to ask the user for permission to use camera and microphone before starting a call. The following method asks for permissions if not already granted.
```Java
public void checkPermissions() {
    List<String> permissionList = new LinkedList<>();
    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
        permissionList.add(Manifest.permission.CAMERA);
    }
    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
        permissionList.add(Manifest.permission.RECORD_AUDIO);
    }
    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
        permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }
    if (permissionList.size() > 0) {
        ActivityCompat.requestPermissions(this, permissionList.toArray(new String[0]), MY_PERMISSIONS_REQUEST);
    } else {
        allPermissionsAccepted = true;
    }
}
```

# AIMES Middleware
The AIMES Middleware resides between the different emergency apps and the PEMEA infrastructure. 
The middleware uses the DigiRoom service to create multimedia rooms for the App API. The middleware 
hosts a Web interface to aid developers trying to integrate their emergency Apps with the AIMES
system. The Web interface can be accessed at: https://mw.aimes-eurostars.eu/.

## App API
The App API is a part of the AIMES system used by developers to integrate with the system. 
The API is hosted by the AIMES Middleware and is HTTPS REST API. The AIMES middleware uses 
closed API to the Ghale AP and to the DigiRoom service. The Ghale AP communication is to 
route the calls made by the App API to the correct PSAP. For the AIMES system this PSAP 
is an internal test PSAP. DigiRoom is used by the middleware to get a webpage that a App 
developer can visit to establish multimedia calls.

All App API calls must be authorized by setting a token in the HTTP authorization header. 
This authorization token is acquired using the registration request in the App API, 
the only API call that does not require authorization.

## Registration
The first step to use the App API is to make the user register. The App would make Register
request to the middleware. The request will contain information about the citizen, for 
example name and phone number. A secret and salt will be returned from the register
request if everything went well. There will also be SMS sent with a token to the citizens phone number. 
To be able to use the rest of the API an authorization token (auth token) must be generated.
The authorization token is generated by taking the information from the registration request and put it through this function:

```
Auth token=md5(secret+salt+SMS token)
```
At last, the generated auth token can be tested using the validate function. A success will be returned 
if the auth token generated is valid. Note that you can either register as a citizen or IoT device. 
An auth token for a citizen registration request can be used for SPIF or Citizen API calls.

